from collections import OrderedDict

size(300,300)

str = "bind billionaires who fly to the upper sky from coming back to earth"
strp = str.replace(" ", "")
spell = "".join(OrderedDict.fromkeys(strp))

bg_col = '#c139b6'
fg_col = '#0215b3'

# fill(None)
# stroke(bg_col)
# ellipsemode(CENTER)
# strokewidth(0.5)

cipher_grid = 3
cell_size = WIDTH/cipher_grid
i = 0

cell_letters = ["ajs", "bkt", "clu", "dmv", "enw", "fox", "gpy", "hqz", "ir"]
cell_positions = []

font("Basteleur")

for x,y in grid(cipher_grid,cipher_grid,cell_size,cell_size):
    #rect(x,y,cell_size,cell_size)
    half_cell = cell_size/2
    cell_pos = (x+half_cell,y+half_cell)
    cell_positions.append(cell_pos)
    # fill(bg_col)
    # ellipse(cell_pos[0], cell_pos[1], 5, 5)
    # text(cell_letters[i], x+5, y+15)
    # i += 1
    fill(None)

# Sigil line

stroke(fg_col)
strokewidth(2)

spell_cells = []

for letter in spell:
    for cell in cell_letters:
        if letter in cell:
            spell_cells.append(cell_letters.index(cell))

first_letter = spell_cells.pop(0)
first_pos = cell_positions[first_letter]

autoclosepath(close=False)
beginpath(first_pos[0],first_pos[1])

for cell in spell_cells:
    pos = cell_positions[cell]
    lineto(pos[0], pos[1])

endpath()
