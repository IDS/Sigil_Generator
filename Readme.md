
# Sigil Generator

Generates a graphical sigil on a 3x3 cipher grid from a spell or phrase.[^1]

In collaboration with [Lucile Olympe Haute](http://lucilehaute.fr), Institute of Diagram Studies High Priestess for Cyber-Witchcraft.

Created to make [this sigil](bindloareswhfytupkmcg/), but it can use any phrase, spell, or affirmation:

> bind billionaires who fly to the upper sky from coming back to earth

![drawing of the sigil on a 3x3 grid](bindloareswhfytupkmcg/full_sigil.svg)

[^1]: Jackson, M.B. *Sigils, Ciphers and Scripts: History and Graphic Function of Magick Symbols.* Somerset, England: Green Magic, 2013.

## Installation

The Python scripts are written for [Shoebot](http://shoebot.net/), follow [instructions](https://docs.shoebot.net/install.html) to install it.

(Shoebot is a rewrite of [Nodebox 1](https://www.nodebox.net/code/index.php/Home) so the scripts might work in there and/or in [Drawbot](https://www.drawbot.com/) with some adjustments)

Additional dependencies:

- [Atom](https://atom.io/) code editor with the [Shoebot extension](https://docs.shoebot.net/extensions.html#atom-extension) (optional, Shoebot needs to be installed first for this to work)
- [Basteleur](https://www.velvetyne.fr/fonts/basteleur/) font from Velvetyne

## Usage

### Sigil generator

[`full_sigil.py`](./full_sigil.py) generates a diagram on a 3x3 Grid.

1. Initial statement phrase e.g. "bind billionaires who fly to the upper sky from coming back to earth"

⬇️

2. Strip the phrase of duplicate letters (see [`sigil_letters.py`](./sigil_letters.py)) e.g. "bindloareswhfytupkmcg"

⬇️

3. Connect the letters on this grid

![The 3x3 letter grid](full_sigil_grid.svg)

⬇️

4. Result

![Sigil line on top of the grid](bindloareswhfytupkmcg/full_sigil.svg)

### Animation

To produce animations, use the [`plain_sigil.py`](./plain_sigil.py) script. 

1. Save the output as `.svg` like [`plain_sigil.svg`](./plain_sigil.svg)
2. Copy the `<path>` part like the one below and paste it to replace the similar one in [`index.html`](animation/index.html)

```svg
<path style="fill:none;stroke-width:2;stroke-linecap:butt;stroke-linejoin:miter;stroke:rgb(0.784314%,8.235294%,70.196078%);stroke-opacity:1;stroke-miterlimit:10;" d="M 150 50 L 250 250 L 150 150 L 50 150 L 250 50 L 250 150 L 50 50 L 250 250 L 50 50 L 150 150 L 150 250 L 250 150 L 50 250 L 150 50 L 250 50 L 50 250 L 150 50 L 50 150 L 250 50 L 50 250 "/>
```

1. Adjust the path length as indicated in the [`index.html`](animation/index.html) file comments so the animation draws the whole line.
2. Open [`index.html`](animation/index.html) in a browser.

![](animation/animation_dark.gif)