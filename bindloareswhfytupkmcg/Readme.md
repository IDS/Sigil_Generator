
# bindloareswhfytupkmcg

![](full_sigil.svg)

This sigil is a talisman to protect Earth from space-faring billionaires. It blocks them from coming back once they have left the planet's surface. To summon this spell, circulate the sigil and the following sentence as widely as possible immediately following the 🚀🍆 rocket lift-off:

> ✨ 🔮 🚀 🍆 🌌 ⍖ 🌏 🌎 🌍 🔮 ✨  Bind Billionaires who fly to the upper sky from coming back to Earth

As you summon, focus all energies on the landing site (e.g. [31.4515199,-104.7635697](https://goo.gl/maps/GzgDVngnFreEveC58) for Blue Origin)
